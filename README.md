# Starter project ionic #

Deze repo bevat een starter-project voor de kennissessie over ionic.
Het starter-project is gebaseerd op het standaard "tutorial" starter project van ionic, met een paar toevoegingen.

## Presentaties ##

Naast het starter project bevat deze repo de bijbehorende presentaties (powerpoint/pdf). Deze staan in het mapje
"presentaties".

## User stories ##

Ter inspiratie staan er in de issue tracker van deze repository een aantal user stories waar je aan zou kunnen werken,
met een korte beschrijving om je op weg te helpen.

