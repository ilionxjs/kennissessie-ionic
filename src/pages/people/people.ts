import {Component} from '@angular/core';

import {NavController, NavParams} from 'ionic-angular';

import {ItemDetailsPage} from '../item-details/item-details';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'page-people',
  templateUrl: 'people.html'
})
export class PeoplePage {

  items: Array<Person>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient) {
    this.http.get<SwResponse<Person>>('https://swapi.co/api/people/').subscribe(swResponse => {
      this.items = swResponse.results;
    });
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }
}
