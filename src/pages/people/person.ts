interface Person {

  name: string;
  height: number;
  mass: number;
  hair_color: string;

  // etc.

}
